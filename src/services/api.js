import axios from 'axios';

const api = axios.create({
  baseURL: 'https://live-ken.herokuapp.com',
});

export default api;
