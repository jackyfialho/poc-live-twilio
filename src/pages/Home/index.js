import React, { useState, useCallback } from 'react';

import api from '../../services/api';

import StartForm from '../../components/StartForm';
import Room from '../../components/Room';

const Home = () => {  
  let jwt;

  const [userName, setUserName] = useState('');  
  const [roomName, setRoomName] = useState('');
  const [token, setToken] = useState(null);  

  async function handleSubmit(event) {
    event.preventDefault();
    if(userName) {
      const response = await api.get(`token/${userName}`);
      jwt = response.data.token;
      setRoomName('LiveDevsPupilla');
      setToken(jwt);
    }    
  }

  const handleUserNameChange = useCallback(event => {
    setUserName(event.target.value);
  }, []);


  return (
    <>
    {!token ?
      <StartForm handleUserNameChange={handleUserNameChange} handleSubmit={handleSubmit}/>
      :
      <Room roomName={roomName} userName={userName} token={token} setToken={setToken} />
    }
    </>
  );
}

export default Home;
