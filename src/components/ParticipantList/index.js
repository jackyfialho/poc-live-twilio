import React from 'react';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { Scrollbars } from 'react-custom-scrollbars';

import './styles.css';

function ParticipantList({ participants, toggleParticipantsList }) {

    var participantListClass = 'participantList';

    if (toggleParticipantsList) {

        participantListClass = 'participantList open';

    }

    var participantList = participants.map(participant =>
        <div key={participant.sid} className="participantList_participant" >
            <AccountCircleIcon className="participantList_icon" />
            <div>
                <p className="participantList_name">{participant.identity}</p>
            </div>
        </div>)

    return (
      <div className={participantListClass}>
          <h3 className="participantList_title">Participants</h3>
          <Scrollbars style={{ width: "100%", height: "100%" }}>
          {participantList}
          </Scrollbars>
      </div>
    );
}

export default ParticipantList;


