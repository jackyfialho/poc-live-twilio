import React from 'react';

import './styles.css';

const StartForm = ({ handleUserNameChange, handleSubmit }) => {

  return (
    <div id="startFormContainer">
      <p>Bem vindo ào primeiro encontro ao vivo do Pupilla!</p>
      <form id="startForm" className="startForm" onSubmit={handleSubmit}>
        <input
          id="name"
          className="startForm_input"
          type="text"
          onChange={handleUserNameChange}
          placeholder="Digite seu nome..."
          required
        />
        <button type="submit" className="startForm_button">Entrar</button>   
      </form>
    </div>
  )
}

export default StartForm;