import React from 'react';
import { makeStyles } from "@material-ui/core";
import IconButton from '@material-ui/core/IconButton';
import MicIcon from '@material-ui/icons/Mic';
import VideocamIcon from '@material-ui/icons/Videocam';
import GroupIcon from '@material-ui/icons/Group';
import CallEndIcon from '@material-ui/icons/CallEnd';
import MicOffIcon from '@material-ui/icons/MicOff';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';
// import ScreenShareIcon from '@material-ui/icons/ScreenShare';
// import StopScreenShareIcon from '@material-ui/icons/StopScreenShare';

import './menu.css';

const useStyles = makeStyles({
  active: {
    marginLeft:"20px",
    color:"white",
    background: 'linear-gradient(45deg, rgb(233, 79, 65) 30%, #ff685a 90%)',
    boxShadow: '0 3px 5px 2px #e33b3a',
    "&:hover": {
      backgroundColor: "solid",
      background: 'linear-gradient(45deg, rgb(233, 79, 65) 30%, #ff685a 90%)',
      boxShadow: '0 3px 5px 2px #e33b3a',
    }
  },
  inactive: {
    color:"white",
    marginLeft:"20px",
    background: '#FF0F0F',
    "&:hover": {
      backgroundColor: "solid",
      background: '#FF0F0F',
    }
  },
  endCall: {
    color:"white",
    marginLeft:"20px",
    background: '#FF0F0F',
    "&:hover": {
      backgroundColor: "solid",
      background: '#FF0F0F',
    }
  }

});
function Menu({ handleCallDisconnect, handleAudioToggle, handleVideoToggle, handleParticipantListToggle, toggleMenu, toggleAudio, toggleVideo, style, handleScreenShare }) {
  const classes = useStyles();
  let menuClass = 'Menu';
  if (toggleMenu) {
    menuClass = 'Menu open';
  }

  return (
    <div className={menuClass} style={style} >
      {toggleAudio ? <IconButton onClick={handleAudioToggle} className={classes.active}>< MicIcon /></IconButton> : <IconButton onClick={handleAudioToggle} className={classes.inactive}><MicOffIcon /></IconButton>}
      {toggleVideo ? <IconButton onClick={handleVideoToggle} className={classes.active}><VideocamIcon /></IconButton> : <IconButton onClick={handleVideoToggle} className={classes.inactive}><VideocamOffIcon /></IconButton>}
      {/* {handleScreenShare ? <IconButton onClick={handleScreenShare} className={classes.active}><ScreenShareIcon /></IconButton> : <IconButton onClick={handleScreenShare} className={classes.inactive}><StopScreenShareIcon /></IconButton>} */}

      <IconButton onClick={handleParticipantListToggle} className={classes.active}>
        <GroupIcon />
      </IconButton>
      <IconButton onClick={handleCallDisconnect} className={classes.endCall}>
        <CallEndIcon />
      </IconButton>

    </div>
  );
}

export default Menu;
