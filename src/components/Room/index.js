import React, { useState, useEffect} from 'react';
import Video from 'twilio-video';

import Participant from '../Participant';
import ParticipantList from '../ParticipantList';
import Menu from '../Menu';

import './styles.css';

const Room = ({ roomName, token, setToken, userName }) => {
  const [room, setRoom] = useState(null);     
  const [participants, setParticipants] = useState([]);
  const [toggleAudio, setToggleAudio] = useState(false);
  const [toggleVideo, setToggleVideo] = useState(false);
  const [toggleParticipantsList, setParticipantsList] = useState(false);
  const [toggleMenu] = useState(false);
  const [toggleFullScreen, setFullScreen] = useState(false);
  const [userVideo, setUserVideo] = useState(true);
  
  function detectWebcam(callback) {
    let md = navigator.mediaDevices;
    if (!md || !md.enumerateDevices) return callback(false);
    md.enumerateDevices().then(devices => {
      console.log(devices);
      callback(devices.some(device => 'videoinput' === device.kind));
    })
  }  

  useEffect(() => {
    detectWebcam(function(hasWebCam) {
      if(hasWebCam === false) {
        setUserVideo(false);
        setToggleVideo(false);
      } 
    });    
  }, [toggleVideo, userVideo]);

  
  useEffect(() => {
    const participantConnected = participant => {      
      setParticipants(prevParticipants => [...prevParticipants, participant]);
    };

    const participantDisconnected = participant => {
        setParticipants(prevParticipants =>
            prevParticipants.filter(p => p !== participant)
        );
    };         

    Video.connect(token, {
        name: roomName,
        identity: userName,
        video: userVideo,
        audio: true        
    }).then(room => {        
        setRoom(room);
        room.on('participantConnected', participantConnected);
        room.on('participantDisconnected', participantDisconnected);
        room.participants.forEach(participantConnected);
        room.localParticipant.tracks.forEach(function (trackPublication) {
          trackPublication.track.disable();
        })
        room.once('disconnected', (room, error) => {
          if (error) {
            console.log('Você foi desconectado:', error.code, error.message);
          }
        });              
    }).catch(error => {
        if ('code' in error) {
          console.error('Falha ao entrar na live:', error.code, error.message);
        }
      });               

    const cleanup = () =>{
      setRoom(currentRoom => {
          if (currentRoom && currentRoom.localParticipant.state === 'connected') {
              currentRoom.localParticipant.tracks.forEach(function (trackPublication) {
                  trackPublication.track.stop();
              });
              currentRoom.disconnect();
              console.log("disconnected")
              return null;
          } else {
              return currentRoom;
          }
      });
    }

    return () => {
        cleanup();
    };
  }, [roomName, token, userName, userVideo]);
  
  function handleCallDisconnect() {
    room.disconnect();
    setToken(null);
  }

  const handleAudioToggle = () => {      
      room.localParticipant.audioTracks.forEach(track => {
          if (track.track.isEnabled) {
              track.track.disable();
          } else {
              track.track.enable();
          }
          setToggleAudio(track.track.isEnabled);
      });
  };

  const handleVideoToggle = () => {
      room.localParticipant.videoTracks.forEach(track => {
          if (track.track.isEnabled) {
              track.track.disable();
          } else {
              track.track.enable();
          }
          setToggleVideo(track.track.isEnabled);
      });
  };

  const handleParticipantListToggle = () => {
      setParticipantsList((prevState) => !prevState);
  }

  const handleFullScreen = (event) => {
    setFullScreen(prevState => !prevState);
    if (toggleFullScreen) {
        event.target.parentElement.style.position = "absolute";
        event.target.parentElement.style.zIndex = "3"
    }
    else {
        event.target.parentElement.style.position = "relative";
        event.target.parentElement.style.zIndex = "1"
    }
  }

  const remoteParticipants = participants.map(participant => (
    <Participant key={participant.sid} participant={participant} toggleFullScreen={toggleFullScreen} handleFullScreen={handleFullScreen} />
  ));

  return(
    room ? (
      <div className="roomContainer">
        <p>Bem vindo ao encontro do Pupilla!</p>      
        <div className="room">        
          <div className="participantContainer">
            <Participant key={room.localParticipant.sid} participant={room.localParticipant} toggleVideo={toggleVideo} toggleAudio={toggleAudio} />        
            <Menu
                handleAudioToggle={handleAudioToggle}
                handleVideoToggle={handleVideoToggle}
                handleCallDisconnect={handleCallDisconnect}
                handleParticipantListToggle={handleParticipantListToggle}
                toggleMenu={toggleMenu}
                toggleAudio={toggleAudio}
                toggleVideo={toggleVideo}
            />
          </div>
          {remoteParticipants}
          <ParticipantList key={participants.identity} participants={participants} toggleParticipantsList={toggleParticipantsList}/>
        </div>      
      </div>
    ) :
    <p>Conectando...</p>
  )
}

export default Room;
  